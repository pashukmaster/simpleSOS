package com.bridgeproduction.simplesos.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.telephony.SmsManager
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.bridgeproduction.simplesos.R
import com.bridgeproduction.simplesos.persistence.Note
import com.bridgeproduction.simplesos.util.ViewModelProviderFactory
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.location.*
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity() {
    val PERMISSION_ID = 42
    lateinit var mFusedLocationClient: FusedLocationProviderClient

    lateinit var navController: NavController

    lateinit var superLat: String
    lateinit var superLong: String
    lateinit var mAdView: AdView
    private lateinit var noteViewModel: NoteViewModel

    lateinit var allNotes: List<Note>

    @Inject
    lateinit var viewmodelProviderFactory: ViewModelProviderFactory

    // Method #1
    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navController = findNavController(R.id.container)
        MobileAds.initialize(this) {}
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)
        Log.d("TAG", "testeg")
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        //      getLastLocation()


//        Thread {
//            allNotes = mutableListOf()
//            noteViewModel =
//                ViewModelProvider(this, viewmodelProviderFactory).get(NoteViewModel::class.java)
//            allNotes = noteViewModel.getAllNotesSimpleList()
//            Log.d("TAG", allNotes[0].title.toString())
//        }.start()


        //Floating action button will redirect to Add New Notes Fragment #AddFragment
        fab.setOnClickListener {
            onFloatingClicked()
        }

//        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
//        if (ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_COARSE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED
//        ) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return
//        }
//        locationManager.requestLocationUpdates(
//            LocationManager.GPS_PROVIDER,
//            2000,
//            10f,
//            object : LocationListener {
//                override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {}
//                override fun onProviderEnabled(s: String) {}
//                override fun onProviderDisabled(s: String) {}
//                override fun onLocationChanged(location: Location?) {}
//            })
//        val myLocation: Location =
//            locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER)
//        val longitude: Double = myLocation.getLongitude()
//        val latitude: Double = myLocation.getLatitude()

        //        Log.d("TAG", "longitude$longitude")
        val sub1: Button = findViewById<Button>(R.id.sos)
        sub1.setOnClickListener {
            Thread {
                allNotes = mutableListOf()

                noteViewModel =
                    ViewModelProvider(
                        this,
                        viewmodelProviderFactory
                    ).get(NoteViewModel::class.java)
                allNotes = noteViewModel.getAllNotesSimpleList()

                if(!allNotes.isEmpty()) {
                    getLastLocation()
                    Log.d("TAG", allNotes[0].title.toString())
                } else {
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(
                            this,
                            "Add Cellphones numbers of Helpers",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }.start()

//            getLastLocation()

        }
    }

    fun getSendSmsIntent(phoneNumber: String, content: String?) {
//        val uri = Uri.parse("smsto:$phoneNumber")
//        val intent = Intent(Intent.ACTION_SENDTO, uri)
//        intent.putExtra("sms_body", content)
        if(checkPermissions()) {
            val smsManager = SmsManager.getDefault() as SmsManager

            smsManager.sendTextMessage(phoneNumber, null, content, null, null)
        }
        //return getIntent(intent, true)
    }

    private fun getIntent(intent: Intent, isNewTask: Boolean): Intent? {
        return if (isNewTask) intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) else intent
    }


    // Method #2
    private fun onFloatingClicked() {
        navController.navigate(R.id.action_listFragment_to_addFragment)
        fab.hide()
    }

    // Method #3
    fun showFloatingButton() {
        fab.show()
        fab.visibility = View.VISIBLE
    }

    private fun updateLongText() {
        Log.wtf(null, "updateLongText")
        Log.wtf(null, superLat)
        Log.wtf(null, superLong)

        Toast.makeText(this, superLat, Toast.LENGTH_SHORT).show()
        Toast.makeText(this, superLong, Toast.LENGTH_SHORT).show()

        val data: String =
            "SOS. Help me. My place is: https://maps.google.com/maps?q=".plus(superLat)
                .plus(",").plus(superLong)

        for (e in allNotes) {
            getSendSmsIntent(e.title.toString(), data)
            //openSMS(e.title.toString(), data)
        }
        Toast.makeText(this, "SOS SMS WAS SENT", Toast.LENGTH_SHORT).show()

    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        Log.wtf(null, "HERE1")

        if (checkPermissions()) {
            Log.wtf(null, "HERE2")

            if (isLocationEnabled()) {
                Log.wtf(null, "HERE3")

                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        superLat = location.latitude.toString()
                        superLong = location.longitude.toString()
                        updateLongText()
                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            superLat = mLastLocation.latitude.toString()
            superLong = mLastLocation.longitude.toString()
            updateLongText()
        }

    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.SEND_SMS
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.SEND_SMS
            ),
            PERMISSION_ID
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm: InputMethodManager =
                        getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }
//    private fun openSMS(phone: String, message: String) {
//
//        val values = ContentValues()
//        values.put("address", phone) // phone number to send
//
//        values.put("date", System.currentTimeMillis().toString() + "")
//        values.put("read", "1") // if you want to mark is as unread set to 0
//
//        values.put("type", "2") // 2 means sent message
//
//        values.put("body", "2$message")
//
//        val uri: Uri = Uri.parse("content://sms/sent")
//        val uri2: Uri? = applicationContext.contentResolver.insert(uri, values)
//        val intent = Intent(Intent.ACTION_VIEW, uri2)
//
//        with(intent) {
//            putExtra("address", "+$phone")
//            putExtra("sms_body", "3$message")
//        }
//
//        when {
//            Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT -> {
//                //Getting the default sms app.
//                val defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(applicationContext)
//
//                // Can be null in case that there is no default, then the user would be able to choose
//                // any app that support this intent.
//                if (defaultSmsPackageName != null) intent.setPackage(defaultSmsPackageName)
//
//                startActivity(intent)
//            }
//            else -> startActivity(intent)
//        }
//    }

}